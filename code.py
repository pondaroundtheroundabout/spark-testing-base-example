from pyspark.sql.functions import concat, col, lit


def add_full_name(df):
    """Concatenates first_name and last_name with a space in between and puts it into full_name."""

    return df.withColumn("full_name", concat(col("first_name"), lit(" "), col("last_name")))

import unittest2
from sparktestingbase.sqltestcase import SQLTestCase
from pyspark.sql.utils import AnalysisException

from datetime import datetime

import code
import schemas


TIME_FORMAT = '%Y-%m-%d %H:%M:%S'


class TestMyCode(SQLTestCase):

    def setUp(self):

        self.maxDiff = None
        self.start_time = datetime.now()

        # You can do all your setup here
        # that will be executed before all your tests in this class

        super(TestMyCode, self).setUp()

    def tearDown(self):

        msg = "The test was running between {start} and {end}".format(
            start=self.start_time.strftime(TIME_FORMAT),
            end=datetime.now().strftime(TIME_FORMAT)
        )

        print(msg)  # Just to show that it gets called after every test

        super(TestMyCode, self).tearDown()

    def test_1(self):

        print("running test 1")

    def test_2(self):

        print("running test 2")

    def test_success(self):
        """Test if the creation of full_name is correct."""

        # Set up the source data we apply the function on
        json_source = [
            '{"first_name": "John", "last_name": "Doe", "age": 43}',
            '{"first_name": "Jane", "last_name": "Doe", "age": 34}'
        ]
        source_df = self.sqlCtx.read.json(
            self.sc.parallelize(json_source),
            schema=schemas.src_schema_with_first_and_last_name
        )

        # Set up what we expect
        json_expected = [
            '{"first_name": "John", "last_name": "Doe", "age": 43, "full_name": "John Doe"}',
            '{"first_name": "Jane", "last_name": "Doe", "age": 34, "full_name": "Jane Doe"}'
        ]
        expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(json_expected),
            schema=schemas.result_schema
        )

        # Run the code
        result_df = code.add_full_name(source_df)

        # Assert we got what we expected
        self.assertDataFrameEqual(expected_df, result_df)

    def test_exception(self):
        """Test if the code throws the correct exception when an incorrect DataFrame is provided."""

        json_source = [
            '{"last_name": "Doe", "age": 43}',
            '{"last_name": "Doe", "age": 34}'
        ]
        source_df = self.sqlCtx.read.json(
            self.sc.parallelize(json_source),
            schema=schemas.src_schema_with_only_last_name
        )

        # Make sure we get the correct type of exception
        with self.assertRaises(AnalysisException) as context:

            code.add_full_name(source_df)

        # Make sure we get the exception we expect
        self.assertTrue("cannot resolve '`first_name`'" in str(context.exception))


if __name__ == "__main__":

    unittest2.main()

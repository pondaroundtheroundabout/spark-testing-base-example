from pyspark.sql.types import StructType, StructField, StringType, IntegerType


src_schema_with_first_and_last_name = StructType([
    StructField("first_name", StringType(), True),
    StructField("last_name", StringType(), True),
    StructField("age", IntegerType(), True)
])


src_schema_with_only_last_name = StructType([
    StructField("last_name", StringType(), True),
    StructField("age", IntegerType(), True)
])

result_schema = StructType([
    StructField("first_name", StringType(), True),
    StructField("last_name", StringType(), True),
    StructField("age", IntegerType(), True),
    StructField("full_name", StringType(), True)
])
